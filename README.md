# Strategies

Looking around there are a few strategies I found. The most common and popular appears to be Git Flow.

Options investigated:

- Git Flow
- GitHub Flow
- Cactus Model
- Skullcandy's
- Master Only

## Git Flow

We (currently, in practice) use a modified version of Git Flow:

![Our Current Strategy](imgs/our-stategy.png)

Actual Git Flow is slightly different:

![Git Flow](imgs/gitflow.png)

Specifically, we should be branching potential candidate release branches from master. Usually an amalgam of several commits, to which fixes can be made if needed, and are then merged back into master and deleted.

Currently, we just branch from master when doing a build and deploy.

Hotfixes are simply from master, and get merged back to master and develop (I believe we currently do them from develop?)

## GitHub Flow

This model is similar to the *Git Flow* model. Lightweight and branch-based.

In a nutshell:

- *master* is sacred and should be deployable at any moment
- Branches are **of of master**
- Remote ⇄ Local branch synchonisation
  - Branches are off *master*
  - Have descriptive names
- Use a pull request to *master* when ready
- Someone else reviews, but you are responsible for merging it back
- Deployments of *master* should happen after the merge
- There is no functional difference between a very small feature and a hotfix


![GitHub Flow](imgs/githubflow.png)


## Cactus Model
Attempts to resolve the issue of GitFlow being too unwieldy and cumbersome.

The name is derived from the fact that all branches merge out from a wide trunk (*master*).

There is the potential to break things in this model, as it relies on rebasing instead of merging things.

Key features:

- Uses rebase on merge.
	- Easier bisection
	- Cleaner visual history

![Cactus Model](imgs/cactus.png)


## Skullcandy

A slightly expanded version of the GitHub Flow model, with more of a focus on scheduled releases.

Key features:

- A branch for each deliverable
- Everything off *master*.
- Features and hot fixes are treated the same.
- QA Merged into Master

![Skull Candy](imgs/sc.png)

## Master Only

Everything done straight on the master branch, with no work done anywhere else. Really only feasible for extremely small / single person teams.


![Master Only](imgs/mo.png)

# Conclusion

We should make better use of the release branches, and continue using Git Flow. We should treat release branches as **Production Ready**. Hotfixes should be made on *master* and merged back as appropriate.

The GitHub model looks interesting. Too much potential for bad fixes to make it to production, and makes it difficult to do hot fixes.

Multiple JIRA's can be combined in a potential release candidate branch - riskier JIRA's should get their own release branch, which is deployed and tested as neede (And is then incorporated back into develop and master when satisfactory testing has been completed).

Less complicated branches can be combined (For example, two or three "three line of code" JIRA's may be combined into a release branch, which is then tested and checked).

| Development Stage    | Git Branch                        | Parent Branch   |
| -----------------    | --------------------------------- | --------------- |
| Backlog              |                                   |                 |
| Analysis             |                                   |                 |
| Prioritised To Do    |                                   |                 |
| In Progress          | feature-\<JIRA\>, hotfix-\<JIRA\> | develop, aster         |
| Blocked              | feature-\<JIRA\>, hotfix-\<JIRA\> | develop, master |
| In Review            | feature-\<JIRA\>, hotfix-\<JIRA\> | develop, master |
| Awaiting UAT Release | release-\<JIRA\>, hotfix-\<JIRA\> | develop, master |
| In QA                | release-\<JIRA\>, hotfix-\<JIRA\> | develop, master |
| QA Complete          | release-\<JIRA\>, hotfix-\<JIRA\> | develop, master |
| Handover             | master                            |                 |
| Awaiting Release     | master                            |                 |
| Done                 | master                            |                 |

# Discussion Links
- [A Successful Git Branching Model (Git Flow)](http://nvie.com/posts/a-successful-git-branching-model/)
- [Understanding the GitHub Flow · GitHub Guides](https://guides.github.com/introduction/flow/)
- [A Succesful Git Branching Model Considered Harmful](https://barro.github.io/2016/02/a-succesful-git-branching-model-considered-harmful/)
- [Git - Branching Workflows - Long Running Branches](https://git-scm.com/book/en/v2/Git-Branching-Branching-Workflows#Long-Running-Branches)
- [Git - Branching Workflows - Topic Branches](https://git-scm.com/book/en/v2/Git-Branching-Branching-Workflows#Topic-Branches)
- [What Git Branching Models Work for You? - Stack Overflow](http://stackoverflow.com/questions/2621610/what-git-branching-models-work-for-you)
- [Git Branch Strategy for Small Dev Team - Stack Overflow](http://stackoverflow.com/questions/2428722/git-branch-strategy-for-small-dev-team)
- [Git Workflows That Work](http://blog.endpoint.com/2014/05/git-workflows-that-work.html)
- [GitHub Flow – Scott Chacon](http://scottchacon.com/2011/08/31/github-flow.html)